<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Storage;
use App\Notifications\CompanyCreation;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::all();

        return view('companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'logo' => 'image|mimes:png,jpg,jpeg|dimensions:min_width=100,min_height=100',
            'email' => 'email',
        ]);

        if (!empty($request->logo)) {
            $logoName = time() . '.' . $request->logo->extension();
            $request->logo->move(public_path('storage'), $logoName);
        }

        $company = Company::create([
            'name' => $request->name,
            'email' => $request->email,
            'website' => $request->website,
            'logo' => $logoName ?? null
        ]);

        $company->notify(new CompanyCreation($company));

        return redirect()->route('company.index')->with('status', 'Company created!');

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Company $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        $company->with('employees');
        return view('companies.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Company $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        $employees = $company->employees()->latest()->paginate(10);

        return view('companies.edit', compact('company', 'employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Company $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $request->validate([
            'name' => 'required',
            'logo' => 'image|mimes:png,jpg,jpeg|dimensions:min_width=100,min_height=100',
            'email' => 'email',
        ]);

        if (!empty($request->logo)) {

            if (Storage::disk('public')->exists($company->logo)) {
                Storage::disk('public')->delete($company->logo);
            }

            $logoName = time() . '.' . $request->logo->extension();
            $request->logo->move(public_path('storage'), $logoName);
        }

        $company->update([
            'name' => $request->name,
            'email' => $request->email,
            'website' => $request->website,
            'logo' => $logoName ?? $company->logo
        ]);

        return redirect()->route('company.edit', compact('company'))->with('status', 'Company updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Company $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->employees()->delete();

        $company->delete();

        return redirect()->route('company.index')->with('status', 'Company deleted!');;
    }
}
