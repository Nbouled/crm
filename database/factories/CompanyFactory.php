<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->company(),
            'email' => $this->faker->unique()->email(),
            'logo' => $this->faker->image('public/storage', 100,100,null,false),
            'website' => $this->faker->url(),
        ];
    }
}
